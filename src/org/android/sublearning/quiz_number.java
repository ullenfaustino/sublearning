package org.android.sublearning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.android.adapter.QuizAdapter;
import org.android.model.ModelChoices_A;
import org.android.model.ModelChoices_B;
import org.android.model.ModelChoices_C;
import org.android.model.ModelChoices_D;
import org.android.model.ModelQuestions;
import org.android.utilities.HighScoreDB;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class quiz_number extends Activity {

	ImageView mImageBackground;
	GridView mButtonsGrid;

	ToggleImageView toggler = new ToggleImageView(this);
	HighScoreDB myDB = new HighScoreDB(this);

	int mPosition = 0;
	int mArraylistCount = 0;
	int score = 0;
	String mUserName;

	ArrayList<ModelQuestions> arr_model_question = new ArrayList<ModelQuestions>();
	ArrayList<ModelChoices_A> arr_choices_A = new ArrayList<ModelChoices_A>();
	ArrayList<ModelChoices_B> arr_choices_B = new ArrayList<ModelChoices_B>();
	ArrayList<ModelChoices_C> arr_choices_C = new ArrayList<ModelChoices_C>();
	ArrayList<ModelChoices_D> arr_choices_D = new ArrayList<ModelChoices_D>();

	ArrayList<String> arr_default = new ArrayList<String>();
	ArrayList<String> arr_sel = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_view);
		this.INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(quiz_number.this, quiz_activity.class));
		quiz_number.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	private void INIT() {
		setUpButtonViews();
		final Handler mHandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mHandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						arr_model_question = getQuestion();
						setArrayChoices();
						Log.d("TEST", "ARRAYLIST_COUNT_QUESTIONS: "
								+ arr_model_question.size());
						showUserSaveDialog();
					}
				}, 1000);
			}
		}).start();
	}

	private void setUpButtonViews() {
		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		/* Set Grid Button */
		int grid_width = mScreen_Width;
		int grid_height = (int) (mScreen_Height * 0.40);
		RelativeLayout.LayoutParams grid_param = new RelativeLayout.LayoutParams(
				grid_width, grid_height);
		grid_param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		mButtonsGrid = (GridView) findViewById(R.id.grid_button_choices);
		mButtonsGrid.setLayoutParams(grid_param);
		/* End */
		mImageBackground = (ImageView) findViewById(R.id.learn_letter_img_bg);
	}

	public ArrayList<ModelQuestions> getQuestion() {
		ArrayList<ModelQuestions> arr_question = new ArrayList<ModelQuestions>();
		Resources res = getResources();
		AssetManager assetManager = res.getAssets();
		String[] files;
		try {
			files = assetManager.list("Quiz/Number/number_question");// getFileList
																		// in
			Arrays.sort(files);
			for (int i = 0; i < files.length; i++) {
				String question = files[i].replace(".png", "");

				arr_question.add(new ModelQuestions(question));

				Log.d("TEST", "QuestionName:" + question);
			}
			mArraylistCount = arr_question.size();
			Log.d("TEST", "ARR_MODEL_QUESTION COUNT :" + arr_question.size());
		} catch (IOException e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}
		return arr_question;
	}

	public void setArrayChoices() {
		arr_choices_A = new ArrayList<ModelChoices_A>();
		arr_choices_B = new ArrayList<ModelChoices_B>();
		arr_choices_C = new ArrayList<ModelChoices_C>();
		arr_choices_D = new ArrayList<ModelChoices_D>();
		Resources res = getResources();
		AssetManager assetManager = res.getAssets();
		String[] files;
		try {
			files = assetManager.list("Quiz/Number/number_button");// getFileList
																	// in
			Arrays.sort(files);
			for (int i = 0; i < files.length; i++) {
				String choice = files[i];

				String a, b, c, d;

				if (choice.contains("a_")) {
					a = choice;
					arr_choices_A.add(new ModelChoices_A(a));
					Log.d("TEST", "CHOICES_A:" + a);
				} else if (choice.contains("b_")) {
					b = choice;
					arr_choices_B.add(new ModelChoices_B(b));
					Log.d("TEST", "CHOICES_B:" + b);
				} else if (choice.contains("c_")) {
					c = choice;
					arr_choices_C.add(new ModelChoices_C(c));
					Log.d("TEST", "CHOICES_C:" + c);
				} else if (choice.contains("d_")) {
					d = choice;
					arr_choices_D.add(new ModelChoices_D(d));
					Log.d("TEST", "CHOICES_D:" + d);
				}
			}
			Log.d("TEST", "ARR_MODEL_CHOICES_A COUNT :" + arr_choices_A.size());
			Log.d("TEST", "ARR_MODEL_CHOICES_B COUNT :" + arr_choices_B.size());
			Log.d("TEST", "ARR_MODEL_CHOICES_C COUNT :" + arr_choices_C.size());
			Log.d("TEST", "ARR_MODEL_CHOICES_D COUNT :" + arr_choices_D.size());
		} catch (IOException e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}
	}

	public void LoadQuestion(final int position) {
		final Handler mhandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mhandler.post(new Runnable() {

					@Override
					public void run() {
						try {
							ModelQuestions mModel = arr_model_question
									.get(position);

							toggler.setImageDrawableFromAssets(
									mImageBackground,
									"Quiz/Number/number_question/"
											+ mModel.getQuestion() + ".png");
							String question_name = mModel.getQuestion();
							arr_default = new ArrayList<String>();
							arr_sel = new ArrayList<String>();
							for (int i = 0; i < 4; i++) {
								if (i == 0) {// gets choices A
									int ctr_withoutA = 0;
									for (int ctr_a = 0; ctr_a < arr_choices_A
											.size(); ctr_a++) {
										ModelChoices_A choiceA = arr_choices_A
												.get(ctr_a);
										String a = choiceA.getA();
										if (a.contains(question_name)) {
											if (a.contains("_unsel")) {
												arr_default.add(a);
											} else {
												arr_sel.add(a);
											}
										} else {
											ctr_withoutA += 1;
										}
									}
									if (ctr_withoutA == arr_choices_A.size()) {
										// random button
										Random rand = new Random();
										int value = rand.nextInt(arr_choices_A
												.size());
										ModelChoices_A choice_a = arr_choices_A
												.get(value);
										if (choice_a.getA().contains("_unsel")) {
											arr_default.add(choice_a.getA());
											arr_sel.add(choice_a.getA()
													.replace("_unsel.png",
															"_sel.png"));
										} else {
											arr_default.add(choice_a.getA()
													.replace("_sel.png",
															"_unsel.png"));
											arr_sel.add(choice_a.getA());
										}
									}
								} else if (i == 1) { // gets choices B
									int ctr_withoutB = 0;
									for (int ctr_b = 0; ctr_b < arr_choices_B
											.size(); ctr_b++) {
										ModelChoices_B choiceB = arr_choices_B
												.get(ctr_b);
										String b = choiceB.getB();
										if (b.contains(question_name)) {
											if (b.contains("_unsel")) {
												arr_default.add(b);
											} else {
												arr_sel.add(b);
											}
										} else {
											ctr_withoutB += 1;
										}
									}
									if (ctr_withoutB == arr_choices_B.size()) {
										// random button
										Random rand = new Random();
										int value = rand.nextInt(arr_choices_B
												.size());
										ModelChoices_B choice_b = arr_choices_B
												.get(value);
										if (choice_b.getB().contains("_unsel")) {
											arr_default.add(choice_b.getB());
											arr_sel.add(choice_b.getB()
													.replace("_unsel.png",
															"_sel.png"));
										} else {
											arr_default.add(choice_b.getB()
													.replace("_sel.png",
															"_unsel.png"));
											arr_sel.add(choice_b.getB());
										}
									}
								} else if (i == 2) { // gets choices C
									int ctr_withoutC = 0;
									for (int ctr_c = 0; ctr_c < arr_choices_C
											.size(); ctr_c++) {
										ModelChoices_C choiceC = arr_choices_C
												.get(ctr_c);
										String c = choiceC.getC();
										if (c.contains(question_name)) {
											if (c.contains("_unsel")) {
												arr_default.add(c);
											} else {
												arr_sel.add(c);
											}
										} else {
											ctr_withoutC += 1;
										}
									}
									if (ctr_withoutC == arr_choices_C.size()) {
										// random button
										Random rand = new Random();
										int value = rand.nextInt(arr_choices_C
												.size());
										ModelChoices_C choice_c = arr_choices_C
												.get(value);
										if (choice_c.getC().contains("_unsel")) {
											arr_default.add(choice_c.getC());
											arr_sel.add(choice_c.getC()
													.replace("_unsel.png",
															"_sel.png"));
										} else {
											arr_default.add(choice_c.getC()
													.replace("_sel.png",
															"_unsel.png"));
											arr_sel.add(choice_c.getC());
										}
									}
								} else if (i == 3) {// gets choices D
									int ctr_withoutD = 0;
									for (int ctr_d = 0; ctr_d < arr_choices_D
											.size(); ctr_d++) {
										ModelChoices_D choiceD = arr_choices_D
												.get(ctr_d);
										String d = choiceD.getD();
										if (d.contains(question_name)) {
											Log.d("TEST", "D:= " + d);
											if (d.contains("_unsel")) {
												arr_default.add(d);
											} else {
												arr_sel.add(d);
											}
										} else {
											ctr_withoutD += 1;
										}
									}
									if (ctr_withoutD == arr_choices_D.size()) {
										// random button
										Random rand = new Random();
										int value = rand.nextInt(arr_choices_D
												.size());
										ModelChoices_D choice_d = arr_choices_D
												.get(value);
										if (choice_d.getD().contains("_unsel")) {
											arr_default.add(choice_d.getD());
											arr_sel.add(choice_d.getD()
													.replace("_unsel.png",
															"_sel.png"));
										} else {
											arr_default.add(choice_d.getD()
													.replace("_sel.png",
															"_unsel.png"));
											arr_sel.add(choice_d.getD());
										}
									}
								}
							}

							QuizAdapter adapter = new QuizAdapter(
									quiz_number.this, arr_default, arr_sel,
									"Quiz/Number/number_button/");
							mButtonsGrid.setAdapter(adapter);
							mButtonsGrid.setOnItemClickListener(onChoiceClick);
						} catch (Exception e) {
							Log.d("TEST", "ERROR: " + e.toString());
						}
					}
				});
			}
		}).start();
	}

	private AdapterView.OnItemClickListener onChoiceClick = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Log.d("TEST", "Current Position:= " + mPosition);
			if (mPosition < (mArraylistCount - 1)) {
				ModelQuestions mModel = arr_model_question.get(mPosition);
				String question_name = mModel.getQuestion();
				String selected = arr_sel.get(position);
				if (selected.contains(question_name)) {
					Log.d("TEST", "You are correct!");
					Toast.makeText(getApplicationContext(), "You are Correct!",
							Toast.LENGTH_SHORT).show();
					score += 1;
				} else {
					Log.d("TEST", "You are Wrong!");
					Toast.makeText(getApplicationContext(), "You are Wrong!",
							Toast.LENGTH_SHORT).show();
				}
				mPosition += 1;
				LoadQuestion(mPosition);
			} else {
				Log.d("TEST", "*********QUIZZES FINISHED*************");
				myDB.saveNumberToDB(score, mUserName);
				Intent i = new Intent(quiz_number.this,
						high_score_view_activity.class);
				i.putExtra("title", "Your Score is");
				i.putExtra("score", score);
				i.putExtra("user", mUserName);
				startActivity(i);
				quiz_number.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			}
		}
	};
	
	public void showUserSaveDialog() {
		AlertDialog.Builder mbuilder = new AlertDialog.Builder(quiz_number.this);
		LayoutInflater inflater = LayoutInflater.from(quiz_number.this);
		final View vw = inflater.inflate(R.layout.enter_user_name, null);
		mbuilder.setView(vw);
		mbuilder.setCancelable(false);
		final AlertDialog alert = mbuilder.create();
		alert.show();

		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		int alert_width = (int) (mScreen_Width * 0.70);
		int alert_height = (int) (mScreen_Height * 0.70);
		alert.getWindow().setLayout(alert_width, alert_height);

		Button btnSave = (Button) vw.findViewById(R.id.btn_user_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText input_user = (EditText) vw.findViewById(R.id.editText_user);
				if (input_user.getText().toString() == "") {
					Toast.makeText(getApplicationContext(),
							"Please Enter your name before you proceed!",
							Toast.LENGTH_SHORT).show();
				} else {
					mUserName = input_user.getText().toString();
					alert.dismiss();
					LoadQuestion(0);
				}
			}
		});
	}
}
