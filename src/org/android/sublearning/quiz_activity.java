package org.android.sublearning;

import org.android.utilities.SoundPlayer;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class quiz_activity extends Activity {

	ToggleImageView toggler = new ToggleImageView(this);
	SoundPlayer media = new SoundPlayer(this);

	ImageView mBtnTest;
	ImageView mBtnCount;
	ImageView mBtnGuess;
	ImageView mBtnScores;
	ImageView mBtnOwlLogo;

	private void INIT() {
		setupButtonsView();
		media.playSound("Background_Sound/main_background.mp3", true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);
		INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		quiz_activity.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	@Override
	protected void onResume() {
		super.onResume();
		media = new SoundPlayer(this);
		media.playSound("Background_Sound/main_background.mp3", true);
	}

	private void setupButtonsView() {
		MenuHandler mMenuHandler = new MenuHandler();

		mBtnTest = (ImageView) findViewById(R.id.img_btn_Test);
		toggler.setImageResource(mBtnTest, R.drawable.test_unsel,
				R.drawable.test_sel, mMenuHandler);
		mBtnCount = (ImageView) findViewById(R.id.img_btn_Count);
		toggler.setImageResource(mBtnCount, R.drawable.count_unsel,
				R.drawable.count_sel, mMenuHandler);
		mBtnGuess = (ImageView) findViewById(R.id.img_btn_Guess);
		toggler.setImageResource(mBtnGuess, R.drawable.guess_unsel,
				R.drawable.guess_sel, mMenuHandler);
		mBtnScores = (ImageView) findViewById(R.id.img_btn_Scores);
		toggler.setImageResource(mBtnScores, R.drawable.hiscore_unsel,
				R.drawable.hiscore_sel, mMenuHandler);
		mBtnOwlLogo = (ImageView) findViewById(R.id.img_btn_owlogo);
		toggler.setImageResource(mBtnOwlLogo, R.drawable.btnowl_unsel,
				R.drawable.btnowl_sel, mMenuHandler);
	}

	/* -----------------HOLDS EVENT OF BUTTONS--------------------- */
	public class MenuHandler implements OnClickListener {

		@Override
		public void onClick(View view) {
			if (view == mBtnTest) {
				startActivity(new Intent(quiz_activity.this, quiz_letter.class));
				quiz_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnCount) {
				startActivity(new Intent(quiz_activity.this, quiz_number.class));
				quiz_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnGuess) {
				startActivity(new Intent(quiz_activity.this, quiz_color.class));
				quiz_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnScores) {
				startActivity(new Intent(quiz_activity.this,
						highscore_activity.class));
				quiz_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnOwlLogo) {
				media.playSound("Background_Sound/title_sound.mp3", true);
			}
		}

	}

}
