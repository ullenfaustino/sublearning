package org.android.sublearning;

import org.android.utilities.SoundPlayer;
import org.android.utilities.ToggleImageView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.VideoView;

public class menu_activity extends Activity {

	ToggleImageView toggler = new ToggleImageView(this);
	SoundPlayer media = new SoundPlayer(this);

	ImageView mBtnLetters;
	ImageView mBtnNumbers;
	ImageView mBtnColors;
	ImageView mBtnQuizzes;
	ImageView mBtnPrayers;
	ImageView mBtnOwlLogo;

	@Override
	protected void onResume() {
		super.onResume();
		media = new SoundPlayer(this);
		media.playSound("Background_Sound/main_background.mp3", true);
	}

	private void INIT() {
		setupButtonsView();
		media.playSound("Background_Sound/main_background.mp3", true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		menu_activity.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	private void setupButtonsView() {
		MenuHandler mMenuHandler = new MenuHandler();
		
		mBtnLetters = (ImageView) findViewById(R.id.img_btn_letters);
		toggler.setImageResource(mBtnLetters, R.drawable.btnletters_unsel,
				R.drawable.btnletters_sel, mMenuHandler);
		mBtnNumbers = (ImageView) findViewById(R.id.img_btn_Numbers);
		toggler.setImageResource(mBtnNumbers, R.drawable.btnnumbers_unsel,
				R.drawable.btnnumbers_sel, mMenuHandler);
		mBtnColors = (ImageView) findViewById(R.id.img_btn_Colors);
		toggler.setImageResource(mBtnColors, R.drawable.btncolor_unsel,
				R.drawable.btncolor_sel, mMenuHandler);
		mBtnQuizzes = (ImageView) findViewById(R.id.img_btn_Quizzes);
		toggler.setImageResource(mBtnQuizzes, R.drawable.btnquiz_unsel,
				R.drawable.btnquiz_sel, mMenuHandler);
		mBtnPrayers = (ImageView) findViewById(R.id.img_btn_Prayers);
		toggler.setImageResource(mBtnPrayers, R.drawable.btnprayers_unsel,
				R.drawable.btnprayers_sel, mMenuHandler);
		mBtnOwlLogo = (ImageView) findViewById(R.id.img_btn_owlogo);
		toggler.setImageResource(mBtnOwlLogo, R.drawable.btnowl_unsel,
				R.drawable.btnowl_sel, mMenuHandler);
	}

	/* -----------------HOLDS EVENT OF BUTTONS--------------------- */
	public class MenuHandler implements OnClickListener {

		@Override
		public void onClick(View view) {
			if (view == mBtnLetters) {
				Log.d("TEST", "****VIEW == mBtnLetter*****");
				setupLetterDialog(R.drawable.learn_abc_sel,
						R.drawable.learn_abc_unsel, R.drawable.sing_abc_sel,
						R.drawable.sing_abc_unsel,
						learn_letters_activity.class, "android.resource://"
								+ getPackageName().toString() + "/"
								+ R.raw.abc);
			} else if (view == mBtnNumbers) {
				Log.d("TEST", "****VIEW == mBtnNumbers*****");
				setupLetterDialog(R.drawable.learn_numbers_sel,
						R.drawable.learn_numbers_unsel,
						R.drawable.sing_numbers_sel,
						R.drawable.sing_number_unsel,
						learn_numbers_activity.class, "android.resource://"
								+ getPackageName().toString() + "/"
								+ R.raw.numbers);
			} else if (view == mBtnColors) {
				Log.d("TEST", "****VIEW == mBtnNumbers*****");
				setupLetterDialog(R.drawable.learn_color_sel,
						R.drawable.learn_color_unsel,
						R.drawable.sing_color_sel, R.drawable.sing_color_unsel,
						learn_colors_activity.class, "android.resource://"
								+ getPackageName().toString() + "/"
								+ R.raw.colors);
			} else if (view == mBtnQuizzes) {
				startActivity(new Intent(menu_activity.this,
						quiz_activity.class));
//				menu_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnPrayers) {
				startActivity(new Intent(menu_activity.this,
						prayer_activity.class));
//				menu_activity.this.finish();
				overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			} else if (view == mBtnOwlLogo) {
				media.playSound("Background_Sound/main_background.mp3", true);
			}
		}

	}

	public void setupLetterDialog(int button1_drawable_sel,
			int button1_drawable_unsel, int button2_drawable_sel,
			int button2_drawable_unsel, final Class<?> _class,
			final String video_path) {
		Log.d("TEST", "****setupLetterDialog*****");
		AlertDialog.Builder mbuilder = new AlertDialog.Builder(
				menu_activity.this);
		LayoutInflater inflater = LayoutInflater.from(menu_activity.this);
		View vw = inflater.inflate(R.layout.learning_letters_dialog, null);
		mbuilder.setView(vw);
		mbuilder.setCancelable(true);
		final AlertDialog alert = mbuilder.create();
		alert.show();

		ImageView mBtnLearn = (ImageView) vw.findViewById(R.id.learn_abc_btn);
		this.toggler.setImageResource(mBtnLearn, button1_drawable_unsel,
				button1_drawable_sel, new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						alert.dismiss();
						startActivity(new Intent(menu_activity.this, _class));
						menu_activity.this.finish();
						overridePendingTransition(R.anim.push_down_in,
								R.anim.push_down_out);
					}
				});
		ImageView mBtnPlayVideo = (ImageView) vw
				.findViewById(R.id.Sing_abc_btn);
		this.toggler.setImageResource(mBtnPlayVideo, button2_drawable_unsel,
				button2_drawable_sel, new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						alert.dismiss();
						playSingVideo(video_path);
					}
				});
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void playSingVideo(String path) {
		AlertDialog.Builder mbuilder = new AlertDialog.Builder(
				menu_activity.this);
		LayoutInflater inflater = LayoutInflater.from(menu_activity.this);
		View vw = inflater.inflate(R.layout.sing_abc_video, null);
		mbuilder.setView(vw);
		mbuilder.setCancelable(true);
		final AlertDialog alert = mbuilder.create();
		alert.show();

		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		int alert_width = (int) (mScreen_Width * 0.90);
		int alert_height = (int) (mScreen_Height * 0.95);
		alert.getWindow().setLayout(alert_width, alert_height);

		VideoView mVideo = (VideoView) vw.findViewById(R.id.abc_videoplayer);
		try {
			String path1 = path;
			// MediaController mc = new MediaController(menu_activity.this);
			// mc.setAnchorView(mVideo);
			// mc.setMediaPlayer(mVideo);
			Uri uri = Uri.parse(path1);
			// mVideo.setMediaController(mc);
			mVideo.setVideoURI(uri);
			mVideo.start();
		} catch (Exception e) {
			Log.d("TEST", "ERROR:" + e.toString());
			alert.dismiss();
		}
	}
}
