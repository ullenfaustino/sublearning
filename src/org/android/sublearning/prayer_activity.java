package org.android.sublearning;

import org.android.utilities.SoundPlayer;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class prayer_activity extends Activity {

	ToggleImageView toggler = new ToggleImageView(this);
	SoundPlayer media = new SoundPlayer(this);

	ImageView mBtnMorning;
	ImageView mBtn3oclock;
	ImageView mBtn12oclock;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_prayer);
			INIT();
		} catch (Exception e) {
			Log.d("TEST", "ERROR: " + e.toString());
		}
	}

	private void INIT() {
		setupButtons();
		media.playSound("Background_Sound/main_background.mp3", true);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		prayer_activity.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		media = new SoundPlayer(this);
		media.playSound("Background_Sound/main_background.mp3", true);
	}

	private void setupButtons() {
		try {
			PrayerButton mPrayerButtonHandler = new PrayerButton();

			Display mDisplay = getWindowManager().getDefaultDisplay();
			int mScreen_Width = mDisplay.getWidth();
			int mScreen_Height = mDisplay.getHeight();

			int btn_width = (int) (mScreen_Width * 0.50);
			int btn_height = (int) (mScreen_Height * 0.30);

			/* Setup Relative button parent */
			int parent_width = (int) (mScreen_Width * 0.75);
			int parent_height = (int) (mScreen_Height * 0.75);

			RelativeLayout.LayoutParams parent_param = new RelativeLayout.LayoutParams(
					parent_width, parent_height);
			parent_param.addRule(RelativeLayout.CENTER_IN_PARENT);
			RelativeLayout rel_parent = (RelativeLayout) findViewById(R.id.relative_button_parent);
			rel_parent.setLayoutParams(parent_param);
			/* End */

			/* Setup Morning button */
			// RelativeLayout.LayoutParams morning_param = new
			// RelativeLayout.LayoutParams(
			// btn_width, btn_height);
			// morning_param.addRule(RelativeLayout.CENTER_IN_PARENT);
			mBtnMorning = (ImageView) findViewById(R.id.prayer_btn_morning);
			// mBtnMorning.setLayoutParams(morning_param);
			toggler.setImageResource(mBtnMorning,
					R.drawable.clock_morning_unsel,
					R.drawable.clock_morning_sel, mPrayerButtonHandler);
			/* End */

			/* Setup 3 oclock button */
			// RelativeLayout.LayoutParams clock3_param = new
			// RelativeLayout.LayoutParams(
			// btn_width, btn_height);
			// clock3_param.addRule(RelativeLayout.CENTER_IN_PARENT);
			mBtn3oclock = (ImageView) findViewById(R.id.prayer_btn_threeoclock);
			// mBtn3oclock.setLayoutParams(clock3_param);
			toggler.setImageResource(mBtn3oclock, R.drawable.clock_3_unsel,
					R.drawable.clock_3_sel, mPrayerButtonHandler);
			/* End */

			/* Setup 12 oclock button */
			// RelativeLayout.LayoutParams clock12_param = new
			// RelativeLayout.LayoutParams(
			// btn_width, btn_height);
			// clock12_param.addRule(RelativeLayout.CENTER_IN_PARENT);
			mBtn12oclock = (ImageView) findViewById(R.id.prayer_btn_twelveoclock);
			// mBtn12oclock.setLayoutParams(clock12_param);
			toggler.setImageResource(mBtn12oclock, R.drawable.clock_12_unsel,
					R.drawable.clock_12_sel, mPrayerButtonHandler);
			/* End */
		} catch (Exception e) {
			Log.d("TEST", "ERROR: " + e.toString());
		}
	}

	public class PrayerButton implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (v == mBtnMorning) {
				LoadPrayer(R.drawable.morning_prayer);
			} else if (v == mBtn3oclock) {
				LoadPrayer(R.drawable.threeoclock_prayer);
			} else if (v == mBtn12oclock) {
				LoadPrayer(R.drawable.twelveoclock_prayer);
			}
		}

	}

	public void LoadPrayer(int img_src) {
		AlertDialog.Builder mbuilder = new AlertDialog.Builder(
				prayer_activity.this);
		LayoutInflater inflater = LayoutInflater.from(prayer_activity.this);
		View vw = inflater.inflate(R.layout.prayer_view, null);
		mbuilder.setView(vw);
		mbuilder.setCancelable(true);
		final AlertDialog alert = mbuilder.create();
		alert.show();

		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		int alert_width = (int) (mScreen_Width * 0.95);
		int alert_height = (int) (mScreen_Height * 0.95);
		alert.getWindow().setLayout(alert_width, alert_height);

		ImageView img_prayer = (ImageView) vw.findViewById(R.id.prayer_img);
		img_prayer.setImageResource(img_src);
	}

}
