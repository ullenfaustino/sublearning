package org.android.sublearning;

import java.io.IOException;
import java.util.ArrayList;

import org.android.adapter.LetterShortcutAdapter;
import org.android.model.ModelLetters;
import org.android.utilities.SoundPlayer;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class learn_letters_activity extends Activity {

	ImageView mBtnNext;
	ImageView mBtnPrev;
	ImageView mBtnBack;
	ImageView mBtnSkip;
	ImageView mBtnImage;
	ImageView mImageBackground;

	ToggleImageView toggler = new ToggleImageView(this);
	int mPosition = 0;
	int mArraylistCount = 0;

	ArrayList<ModelLetters> arr_model_letters = new ArrayList<ModelLetters>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.learn_letter);
		this.INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(learn_letters_activity.this,
				menu_activity.class));
		learn_letters_activity.this.finish();
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	private void INIT() {
		setUpButtonViews();
		final Handler mHandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mHandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						arr_model_letters = getFilenameFromAssets();
						LoadLetterImage(0);
						Log.d("TEST",
								"ARRAYLIST_COUNT: " + arr_model_letters.size());
					}
				}, 1000);
			}
		}).start();
	}

	private void setUpButtonViews() {
		NavigationHandler mButtonHandler = new NavigationHandler();

		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		/* Set Next Button */
		int nxt_width = (int) (mScreen_Width * 0.23);
		int nxt_height = (int) (mScreen_Height * 0.23);
		int nxt_left_margin = (int) (mScreen_Width * 0.0030);
		RelativeLayout.LayoutParams next_param = new RelativeLayout.LayoutParams(
				nxt_width, nxt_height);
		next_param.setMargins(0, 0, nxt_left_margin, 0);
		next_param.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		next_param.addRule(RelativeLayout.CENTER_VERTICAL);
		mBtnNext = (ImageView) findViewById(R.id.learn_letter_next);
		mBtnNext.setLayoutParams(next_param);
		toggler.setImageResource(mBtnNext, R.drawable.right_unsel,
				R.drawable.right_sel, mButtonHandler);
		/* End */

		/* Set Prev Button */
		int prev_width = (int) (mScreen_Width * 0.23);
		int prev_height = (int) (mScreen_Height * 0.23);
		int prev_left_margin = (int) (mScreen_Width * 0.0030);
		RelativeLayout.LayoutParams prev_param = new RelativeLayout.LayoutParams(
				prev_width, prev_height);
		prev_param.setMargins(prev_left_margin, 0, 0, 0);
		prev_param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		prev_param.addRule(RelativeLayout.CENTER_VERTICAL);
		mBtnPrev = (ImageView) findViewById(R.id.learn_letter_prev);
		mBtnPrev.setLayoutParams(prev_param);
		toggler.setImageResource(mBtnPrev, R.drawable.left_unsel,
				R.drawable.left_sel, mButtonHandler);
		/* End */

		/* Set Back Button */
		int back_width = (int) (mScreen_Width * 0.30);
		int back_height = (int) (mScreen_Height * 0.15);
		RelativeLayout.LayoutParams back_param = new RelativeLayout.LayoutParams(
				back_width, back_height);
		back_param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		back_param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		mBtnBack = (ImageView) findViewById(R.id.learn_letter_back);
		mBtnBack.setLayoutParams(back_param);
		toggler.setImageResource(mBtnBack, R.drawable.back_unsel,
				R.drawable.back_sel, mButtonHandler);
		/* End */

		/* Set Skip Button */
		int skip_width = (int) (mScreen_Width * 0.30);
		int skip_height = (int) (mScreen_Height * 0.15);
		RelativeLayout.LayoutParams skip_param = new RelativeLayout.LayoutParams(
				skip_width, skip_height);
		skip_param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		skip_param.addRule(RelativeLayout.ABOVE, mBtnBack.getId());
		mBtnSkip = (ImageView) findViewById(R.id.learn_letter_skip);
		mBtnSkip.setLayoutParams(skip_param);
		toggler.setImageResource(mBtnSkip, R.drawable.atoz_unsel,
				R.drawable.atoz_sel, mButtonHandler);
		/* End */

		/* Set Button Image */
		int btn_image_width = (int) (mScreen_Width * 0.45);
		int btn_image_height = (int) (mScreen_Height * 0.45);
		int btn_image_right_margin = (int) (mScreen_Width * 0.0075);
		RelativeLayout.LayoutParams btn_image_param = new RelativeLayout.LayoutParams(
				btn_image_width, btn_image_height);
		btn_image_param.setMargins(0, 0, btn_image_right_margin, 0);
		btn_image_param.addRule(RelativeLayout.CENTER_VERTICAL);
		btn_image_param.addRule(RelativeLayout.LEFT_OF, mBtnNext.getId());
		mBtnImage = (ImageView) findViewById(R.id.learn_letter_btnImg);
		mBtnImage.setLayoutParams(btn_image_param);
		/* End */
		mImageBackground = (ImageView) findViewById(R.id.learn_letter_img_bg);
	}

	/* -----------------HOLDS EVENT OF BUTTONS--------------------- */
	public class NavigationHandler implements OnClickListener {

		@Override
		public void onClick(View view) {
			if (view == mBtnNext) {
				if (mPosition < mArraylistCount) {
					mPosition += 1;
					LoadLetterImage(mPosition);
				}
			} else if (view == mBtnPrev) {
				if (mPosition != 0) {
					mPosition -= 1;
					LoadLetterImage(mPosition);
				}
			} else if (view == mBtnBack) {
				startActivity(new Intent(learn_letters_activity.this,
						menu_activity.class));
				learn_letters_activity.this.finish();
				overridePendingTransition(R.anim.slide_left_right,
						R.anim.slide_right_left);
			} else if (view == mBtnSkip) {
				showSkipButtons();
			} else if (view == mBtnImage) {
				SoundPlayer media = new SoundPlayer(learn_letters_activity.this);
				ModelLetters mModel = arr_model_letters.get(mPosition);
				String name = mModel.getLetterBG().replace(".png", "");
				String audio_name = name.toUpperCase() + ".mp3";
				media.playSound("Letters/letters_sound/" + audio_name, false);
			}
		}
	}

	public void showSkipButtons() {
		AlertDialog.Builder mbuilder = new AlertDialog.Builder(
				learn_letters_activity.this);
		LayoutInflater inflater = LayoutInflater
				.from(learn_letters_activity.this);
		View vw = inflater.inflate(R.layout.grid_dialog_view, null);
		mbuilder.setView(vw);
		mbuilder.setCancelable(true);
		final AlertDialog alert = mbuilder.create();
		alert.show();

		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		int alert_width = (int) (mScreen_Width * 0.97);
		int alert_height = (int) (mScreen_Height * 0.97);
		alert.getWindow().setLayout(alert_width, alert_height);

		GridView mGridbtn = (GridView) vw.findViewById(R.id.grid_dialog);
		LetterShortcutAdapter adapter = new LetterShortcutAdapter(
				learn_letters_activity.this, arr_model_letters);
		mGridbtn.setAdapter(adapter);
		mGridbtn.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mPosition = position;
				alert.dismiss();
				LoadLetterImage(position);
			}
		});
	}

	public ArrayList<ModelLetters> getFilenameFromAssets() {
		ArrayList<ModelLetters> arr_model = new ArrayList<ModelLetters>();
		Resources res = getResources();
		AssetManager assetManager = res.getAssets();
		String[] files;
		try {
			files = assetManager.list("Letters/letters_bg");// getFileList in
															// Assets
			for (int i = 0; i < files.length; i++) {
				String file_name = files[i].replace(".png", "");
				String name_bg = files[i];
				String name_sel = file_name + "_sel.png";
				String name_unsel = file_name + "_unsel.png";

				arr_model.add(new ModelLetters(name_bg, name_sel, name_unsel));

				// Log.d("TEST", "name_bg:" + name_bg);
				// Log.d("TEST", "name_sel:" + name_sel);
				// Log.d("TEST", "name_unsel:" + name_unsel);
			}
			mArraylistCount = arr_model.size();
			Log.d("TEST", "ARR_MODEL COUNT :" + arr_model.size());
		} catch (IOException e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}
		return arr_model;
	}

	public void LoadLetterImage(final int position) {
		final Handler mhandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mhandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						try {
							NavigationHandler mButtonHandler = new NavigationHandler();
							ModelLetters mModel = arr_model_letters
									.get(position);

							toggler.setImageDrawableFromAssets(
									mImageBackground, "Letters/letters_bg/"
											+ mModel.getLetterBG(),
									mButtonHandler);
							toggler.setImageFromAssets(
									mBtnImage,
									"Letters/letters_button/"
											+ mModel.getLetterBtnUnsel(),
									"Letters/letters_button/"
											+ mModel.getLetterBtnSel(),
									mButtonHandler);
						} catch (Exception e) {
							Log.d("TEST", "ERROR: " + e.toString());
						}
					}
				}, 250);
			}
		}).start();
	}
}
