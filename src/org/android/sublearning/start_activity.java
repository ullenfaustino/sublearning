package org.android.sublearning;

import org.android.utilities.SoundPlayer;
import org.android.utilities.ToggleImageView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class start_activity extends Activity {

	ToggleImageView toggler = new ToggleImageView(this);
	SoundPlayer media = new SoundPlayer(this);

	ImageView mBtnStart;

	private void INIT() {
		setupButtonSize();
		media.playSound("Background_Sound/title_sound.mp3", true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		this.INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		media.playSound("Background_Sound/title_sound.mp3", true);
	}

	private View.OnClickListener onStartClick = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			startActivity(new Intent(start_activity.this, menu_activity.class));
			overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
		}
	};

	private void setupButtonSize() {
		Display mDisplay = getWindowManager().getDefaultDisplay();
		int mScreen_Width = mDisplay.getWidth();
		int mScreen_Height = mDisplay.getHeight();

		int btnWidth = (int) (mScreen_Width * 0.50);
		int btnHeight = (int) (mScreen_Height * 0.30);
		int btnMarginBottom = (int) (mScreen_Height * 0.10);

		RelativeLayout.LayoutParams start_param = new RelativeLayout.LayoutParams(
				btnWidth, btnHeight);
		start_param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		start_param.addRule(RelativeLayout.CENTER_IN_PARENT);
		start_param.setMargins(0, 0, 0, btnMarginBottom);
		mBtnStart = (ImageView) findViewById(R.id.btnStart);
		mBtnStart.setLayoutParams(start_param);
		toggler.setImageResource(mBtnStart, R.drawable.btnstart_unsel,
				R.drawable.btnstart_sel, onStartClick);
	}
}
