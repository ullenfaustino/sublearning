package org.android.sublearning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class high_score_view_activity extends Activity {

	String mTitle, mTxtUser;
	int mScore;

	TextView mTxtTitle, mTxtScore, mTxtUserName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_view);
		INIT();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
		startActivity(new Intent(high_score_view_activity.this,
				quiz_activity.class));
		overridePendingTransition(R.anim.slide_left_right,
				R.anim.slide_right_left);
	}

	private void INIT() {
		final Handler mHandler = new Handler();
		new Thread(new Runnable() {

			@Override
			public void run() {
				mHandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						mTitle = getIntent().getExtras().getString("title");
						mScore = getIntent().getExtras().getInt("score");
						mTxtUser = getIntent().getExtras().getString("user");

						mTxtTitle = (TextView) findViewById(R.id.txt_score_title);
						mTxtScore = (TextView) findViewById(R.id.txt_score);
						mTxtUserName = (TextView) findViewById(R.id.txt_score_user);

						mTxtTitle.setText(mTitle);
						mTxtScore.setText(String.valueOf(mScore));
						mTxtUserName.setText(mTxtUser);
					}
				}, 450);
			}
		}).start();
	}
}
