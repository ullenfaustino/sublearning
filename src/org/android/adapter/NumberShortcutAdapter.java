package org.android.adapter;

import java.util.ArrayList;

import org.android.model.ModelNumbers;
import org.android.sublearning.R;
import org.android.utilities.ToggleImageView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class NumberShortcutAdapter extends BaseAdapter {

	LayoutInflater inflater;
	ArrayList<ModelNumbers> arr_number;
	ToggleImageView toggler;

	public NumberShortcutAdapter(Context ctx, ArrayList<ModelNumbers> data) {
		this.inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arr_number = data;
		this.toggler = new ToggleImageView(ctx);
	}

	@Override
	public int getCount() {
		return arr_number.size();
	}

	@Override
	public Object getItem(int pos) {
		return pos;
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View vw = view;

		if (vw == null) {
			vw = inflater.inflate(R.layout.grid_button_image, parent, false);
		}

		ImageView img = (ImageView) vw.findViewById(R.id.grid_image_btn);

		ModelNumbers mModel = arr_number.get(position);
		String name = mModel.getNumberBG().replace(".png", "");
		Log.d("TEST", "name: " + name);

		String name_unsel = "Numbers/numbers_shortcut/" + name + "_unsel.png";
		String name_sel = "Numbers/numbers_shortcut/" + name + "_sel.png";
		Log.d("TEST", "name_unsel: " + name_unsel);
		Log.d("TEST", "name_sel: " + name_sel);
		
		toggler.setImageFromAssets(img, name_unsel, name_sel);
		return vw;
	}

}
