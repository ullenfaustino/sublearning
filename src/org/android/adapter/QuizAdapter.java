package org.android.adapter;

import java.util.ArrayList;

import org.android.sublearning.R;
import org.android.utilities.ToggleImageView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class QuizAdapter extends BaseAdapter {

	LayoutInflater inflater;
	ToggleImageView toggler;
	Context mContext;
	String uri;
	ArrayList<String> arr_choice_default, arr_choice_sel;

	public QuizAdapter(Context ctx, ArrayList<String> img_data_default,
			ArrayList<String> img_data_sel, String asset_uri) {
		this.mContext = ctx;
		this.inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arr_choice_default = img_data_default;
		this.arr_choice_sel = img_data_sel;
		this.toggler = new ToggleImageView(ctx);
		this.uri = asset_uri;
	}

	@Override
	public int getCount() {
		return arr_choice_default.size();
	}

	@Override
	public Object getItem(int pos) {
		return pos;
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View vw = view;

		if (vw == null) {
			vw = inflater.inflate(R.layout.grid_button_image, parent, false);
		}

		ImageView img = (ImageView) vw.findViewById(R.id.grid_image_btn);

		String img_drawable_default = arr_choice_default.get(position);
		String img_drawable_sel = arr_choice_sel.get(position);
		toggler.setImageFromAssets(img, uri + img_drawable_default, uri
				+ img_drawable_sel);
		return vw;
	}

}
