package org.android.utilities;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class ToggleImageView {
	Context mContext;
	Activity mActivity;

	public ToggleImageView(Context context) {
		this.mContext = context;
		this.mActivity = (Activity) context;
	}

	public void setImageResource(ImageView img, int img_state_default,
			int img_state_pressed, View.OnClickListener click_listener) {
		StateListDrawable states = new StateListDrawable();
		states.addState(new int[] { -android.R.attr.state_pressed }, mContext
				.getResources().getDrawable(img_state_default));
		states.addState(new int[] {},
				mContext.getResources().getDrawable(img_state_pressed));
		img.setImageDrawable(states);
		img.setOnClickListener(click_listener);
	}

	public void setImageFromAssets(ImageView img, String file_path_default,
			String file_path_pressed, View.OnClickListener click_listener) {
		try {
			Drawable draw_default = Drawable.createFromStream(mContext
					.getAssets().open(file_path_default), null);
			Drawable draw_pressed = Drawable.createFromStream(mContext
					.getAssets().open(file_path_pressed), null);
			StateListDrawable states = new StateListDrawable();
			states.addState(new int[] { -android.R.attr.state_pressed },
					draw_default);
			states.addState(new int[] {}, draw_pressed);
			// Log.d("TEST", "DRAWABLE UNSEL: " + draw_default);
			// Log.d("TEST", "DRAWABLE SEL: " + draw_pressed);
			img.setImageDrawable(states);
			img.setOnClickListener(click_listener);
		} catch (Exception e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}
	}

	public void setImageFromAssets(ImageView img, String file_path_default,
			String file_path_pressed) {
		try {
			Drawable draw_default = Drawable.createFromStream(mContext
					.getAssets().open(file_path_default), null);
			Drawable draw_pressed = Drawable.createFromStream(mContext
					.getAssets().open(file_path_pressed), null);
			StateListDrawable states = new StateListDrawable();
			states.addState(new int[] { -android.R.attr.state_pressed },
					draw_default);
			states.addState(new int[] {}, draw_pressed);
			// Log.d("TEST", "DRAWABLE UNSEL: " + draw_default);
			// Log.d("TEST", "DRAWABLE SEL: " + draw_pressed);
			img.setImageDrawable(states);
		} catch (Exception e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}
	}

	public void setImageDrawableFromAssets(ImageView img, String img_src_path,
			View.OnClickListener click_listener) {
		try {
			Drawable draw_default = Drawable.createFromStream(mContext
					.getAssets().open(img_src_path), null);
			// Log.d("TEST", "DRAWABLE: " + draw_default);
			img.setImageDrawable(draw_default);
			img.setOnClickListener(click_listener);
		} catch (IOException e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}

	}

	public void setImageDrawableFromAssets(ImageView img, String img_src_path) {
		try {
			Drawable draw_default = Drawable.createFromStream(mContext
					.getAssets().open(img_src_path), null);
			// Log.d("TEST", "DRAWABLE: " + draw_default);
			img.setImageDrawable(draw_default);
		} catch (IOException e) {
			Log.d("TEST", "ERROR: " + e.toString());
			e.printStackTrace();
		}

	}
}
