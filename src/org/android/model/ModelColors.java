package org.android.model;

public class ModelColors {

	private String ColorBG, ColorBtnSel, ColorBtnUnsel;

	public ModelColors(String colorBg, String colorBtnSel, String colorBtnUnsel) {
		setColorBG(colorBg);
		setColorBtnSel(colorBtnSel);
		setColorBtnUnsel(colorBtnUnsel);
	}

	public String getColorBG() {
		return ColorBG;
	}

	public void setColorBG(String colorBG) {
		ColorBG = colorBG;
	}

	public String getColorBtnSel() {
		return ColorBtnSel;
	}

	public void setColorBtnSel(String colorBtnSel) {
		ColorBtnSel = colorBtnSel;
	}

	public String getColorBtnUnsel() {
		return ColorBtnUnsel;
	}

	public void setColorBtnUnsel(String colorBtnUnsel) {
		ColorBtnUnsel = colorBtnUnsel;
	}

}
