package org.android.model;


public class ModelLetters {

	private String LetterBG, LetterBtnSel, LetterBtnUnsel;

	public ModelLetters(String letterBg, String letterBtnSel,
			String letterBtnUnsel) {
		setLetterBG(letterBg);
		setLetterBtnSel(letterBtnSel);
		setLetterBtnUnsel(letterBtnUnsel);
	}

	public String getLetterBG() {
		return LetterBG;
	}

	public void setLetterBG(String letterBG) {
		LetterBG = letterBG;
	}

	public String getLetterBtnSel() {
		return LetterBtnSel;
	}

	public void setLetterBtnSel(String letterBtnSel) {
		LetterBtnSel = letterBtnSel;
	}

	public String getLetterBtnUnsel() {
		return LetterBtnUnsel;
	}

	public void setLetterBtnUnsel(String letterBtnUnsel) {
		LetterBtnUnsel = letterBtnUnsel;
	}

}
